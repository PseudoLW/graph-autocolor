var cytoscape = require('cytoscape')
var edgehandles = require('cytoscape-edgehandles')

cytoscape.use( edgehandles )

$(function() {
  var cy = cytoscape({
    container: $('#cy'),

    elements: [ // list of graph elements to start with
      { // node a
        data: { id: 'a' }
      },
      { // node b
        data: { id: 'b' }
      },
      { // edge ab
        data: { id: 'ab', source: 'a', target: 'b' }
      }
    ],
    style: [
      {
        selector: 'edge',
        style: {
          'curve-style': 'bezier',
          'target-arrow-shape': 'none'
        }
      },

      // some style for the extension

      {
        selector: '.eh-handle',
        style: {
          'background-color': 'black',
          'width': 12,
          'height': 12,
          'shape': 'ellipse',
          'overlay-opacity': 0,
          'border-width': 12, // makes the handle easier to hit
          'border-opacity': 0
        }
      },

      {
        selector: '.eh-hover',
        style: {
          'background-color': 'red'
        }
      },

      {
        selector: '.eh-source',
        style: {
          'border-width': 2,
          'border-color': 'red'
        }
      },

      {
        selector: '.eh-target',
        style: {
          'border-width': 2,
          'border-color': 'red'
        }
      },

      {
        selector: '.eh-ghost-edge.eh-preview-active',
        style: {
          'opacity': 0
        }
      }
    ]
    
  });
  cy.reset()
  cy.resize()

  var eh = cy.edgehandles({
    edgeType: function( sourceNode, targetNode ) {
      return sourceNode.edgesWith( targetNode ).empty() ? 'flat' : null;
    },
  })

  var nodeMode = true
  var edgeMode = false
  var delMode = false
  

  $('#cy').mouseenter(function() {
    if($('#nodeMode').hasClass('active')) {
      eh.disableDrawMode()
      eh.disable()
      nodeMode = true
    } else nodeMode = false
    
    if($('#edgeMode').hasClass('active')) {
      eh.enableDrawMode()
      eh.enable()
      edgeMode = true
    } else edgeMode = false
    
    if($('#delMode').hasClass('active')) {
      eh.disableDrawMode()
      eh.disable()
      delMode = true
    } else delMode = false
  })

  
    cy.on("tap", function(e) {
      if(nodeMode === true) {
        cy.add([{
            group: "nodes",
            id: "testid",
            renderedPosition: {
                x: e.renderedPosition.x,
                y: e.renderedPosition.y,
            }
        }])
      }
    })
  
  
  cy.on("tap", "node", function(e) {
    if(delMode === true) {
      cy.remove(cy.$id(this.id()))
    }
    
  })

  cy.on("tap", "edge", function(e) {
    if(delMode === true) {
      cy.remove(cy.$id(this.id()))
    }
  })

  $('#but').click(function(e) {
    e.preventDefault()
    var colorVal = $("#numOfColor").val()
    var cyVal = cy.elements().json()
    $.ajax({
      type: "POST",
      url: "/solve",
      data: {
        elements: cyVal,
        color: colorVal
      },
      dataType: "json",
      success: function(result) {
        alert('ok')
        resArray = Object.keys( result )
        if(resArray.length === 0) {
          $('result').text('Solusi tidak ditemukan')
        }
        else {
          $('result').text('Solusi ditemukan')
          for(i = 0; i < resArray.length; i++) {
            cy.nodes('[id = "' + resArray[i] + '"]').style('background-color', result[resArray[i]]).update()
          }
        }
      },
      error: function(result) {
        $('#result').text('Something is wrong with the server')
      }
    })
  })
})